package fr.afpa.cda.vue;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.util.Scanner;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import fr.afpa.cda.beans.Utilisateur;
import fr.afpa.cda.controle.Controle;

public class Login extends JFrame implements ActionListener, TextListener {
	/**
	 * methode de connexion demander à l'utilisateur de tapper son login et mot de
	 * passe afficher bonjour nom prenom si la connexion est ok
	 * 
	 * @author Sofiane
	 */
	private JLabel ErrorMessage = new JLabel("");
	private TextField saisieLogin = new TextField(12);
	private JPasswordField pass = new JPasswordField(9);
	public static Utilisateur verifUtil;
	JFrame login = new JFrame("CDA Messagerie");

	public void seConecter() {
		// this.setLocationRelativeTo(null);
		JButton creatCompte = new JButton("Création de compte");
		JButton conect = new JButton("Connexion");
		JLabel log = new JLabel("  Login                 ");
		JLabel mdp = new JLabel("Mot de passe ");

		JPanel panel = new JPanel();
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JPanel panel4 = new JPanel();

		panel.setLayout(new GridBagLayout());

		// panel.setLayout(new GridBagLayout());

		panel1.setLayout(new FlowLayout());
		panel1.add(log);
		panel1.add(saisieLogin);

		panel2.setLayout(new FlowLayout());
		panel2.add(mdp);
		panel2.add(pass);
		panel3.setLayout(new FlowLayout());
		panel3.add(creatCompte);
		panel3.add(conect);

		// GridBagConstraints gc = new GridBagConstraints();
		// ErrorMessage.setVisible(false);
		panel.add(new Label("CDA 20156 - Messagerie"));
		panel4.add(ErrorMessage);

		this.setLayout(new GridLayout(5, 1));
		this.add(panel);
		this.add(panel4);
		this.add(panel1);
		this.add(panel2);
		this.add(panel3);

		// this.setBounds(10, 10, 10, 50);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setSize(400, 400);
		this.setVisible(true);
		this.setBackground(Color.BLACK);

		creatCompte.addActionListener(this);
		conect.addActionListener(this);
		// saisieLogin.addTextListener(this);

	}

	@Override
	public void textValueChanged(TextEvent arg0) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {
		String source = ((JButton) e.getSource()).getLabel();
		if ("Connexion".equals(source)) {
			if (saisieLogin.getText().isEmpty() || pass.getText().isEmpty()) {
				ErrorMessage.setForeground(new Color(255, 0, 0));
				ErrorMessage.setText("Veuillez remplir login/ mot de passe !");
			} else {
				Controle controle = new Controle();
				setVerifUtil(controle.verifConnexion(saisieLogin.getText(), pass.getText()));
				if (getVerifUtil() == null) {
					saisieLogin.setText("");
					pass.setText("");
					ErrorMessage.setForeground(new Color(255, 0, 0));
					ErrorMessage.setText("Login ou mot de passe incorrect !!");

				} else {
					this.dispose();

					BoiteReception boiteRecep = new BoiteReception(verifUtil);
					// ErrorMessage.setText("Bonjour "+ verifUtil.getNom()+"
					// "+verifUtil.getPrenom());
				}
			}
		} else if ("Création de compte".equals(source)) {
			this.dispose();
			CreationCompte creat = new CreationCompte();
			creat.fenetreCre();
		}
	}

	public static Utilisateur getVerifUtil() {
		return verifUtil;
	}

	public static void setVerifUtil(Utilisateur verifUtil) {
		Login.verifUtil = verifUtil;
	}
}
