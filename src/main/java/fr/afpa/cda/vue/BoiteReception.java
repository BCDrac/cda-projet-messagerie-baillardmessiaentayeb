package fr.afpa.cda.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fr.afpa.cda.beans.Message;
import fr.afpa.cda.beans.Utilisateur;
import fr.afpa.cda.controle.Controle;

/**
 * Classe gérant la boîte de réception : messages reçus, envoyés et recherche de
 * messages reçus/envoyés
 * 
 * @author Cécile
 */
public class BoiteReception extends JFrame implements ActionListener, MouseListener {

	Controle controle = new Controle();
	Utilisateur utilisateur = new Utilisateur();
	JTextField barreRecherche = new JTextField(10);
	JLabel ErrorMessage = new JLabel("");
	JTextArea auteur = new JTextArea(10, 10);
	JTextArea titre = new JTextArea(10, 10);
	JTextArea contenu = new JTextArea(10, 10);
	JFrame frame = new JFrame("CDA Messagerie");

	public BoiteReception(Utilisateur utilisateur) {

		this.utilisateur = utilisateur;

		frame.setResizable(false);
		frame.setSize(600, 600);

		// ---------------Menu---------------------------
		JMenuBar menu = new JMenuBar();
		JMenu fichier = new JMenu("Fichier");
		JMenu compte = new JMenu("Compte");
		JMenu help = new JMenu("Help");
		JMenuItem nouveauMess = new JMenuItem("Nouveau Message");
		nouveauMess.setName("nouveau");
		nouveauMess.addActionListener(this);
		JMenuItem deconnexion = new JMenuItem("Déconnexion");
		deconnexion.setName("deco");
		deconnexion.addActionListener(this);
		JMenuItem voirCompte = new JMenuItem("Voir le compte");
		voirCompte.setName("voirCompte");
		voirCompte.addActionListener(this);
		JMenuItem editCompte = new JMenuItem("Éditer le compte");
		editCompte.setName("editerCompte");
		editCompte.addActionListener(this);

		menu.add(fichier);
		menu.add(compte);
		menu.add(help);
		compte.add(voirCompte);
		compte.add(editCompte);
		fichier.add(nouveauMess);
		fichier.add(deconnexion);

		frame.setJMenuBar(menu);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// ---------------Label de bienvenue---------
		JPanel label = new JPanel();
		JLabel bienvenue = new JLabel("Bonjour " + this.utilisateur.getNom() + " " + this.utilisateur.getPrenom());
		label.add(bienvenue);
		frame.add(label, BorderLayout.PAGE_START);

		JTabbedPane onglets = new JTabbedPane();
		onglets.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		onglets.setBounds(30, 30, 200, 200);

		// ---------------Onglet boîte de réception----------
		onglets.add("Boîte de Réception", creerMessagesRecus());

		// ---------------Onglet messages envoyés-----------
		onglets.add("Messages envoyés", creerMessagesEnvoyes());

		// ---------------Onglet recherche de mail----------

		onglets.add("Recherche de mail", creerRechercheMessages());

		// -----------------------------------------
		onglets.setBackgroundAt(0, Color.LIGHT_GRAY);
		onglets.setBackgroundAt(1, Color.LIGHT_GRAY);
		onglets.setBackgroundAt(2, Color.LIGHT_GRAY);
		onglets.setSize(400, 300);
		onglets.setBorder(BorderFactory.createMatteBorder(20, 20, 200, 20, getBackground()));

		frame.add(onglets, BorderLayout.CENTER);
		frame.setSize(800, 600);
		frame.setVisible(true);
		nouveauMess.addActionListener(this);
		deconnexion.addActionListener(this);
		voirCompte.addActionListener(this);
	}

	/**
	 * Crée la partie de boîte de messages reçus
	 * 
	 * @return JPanel
	 * 
	 * @author Cécile
	 */
	private JPanel creerMessagesRecus() {

		JPanel boiteRecept = new JPanel(new BorderLayout());

		JTextArea texteReceptAuteur = new JTextArea(10, 10);
		texteReceptAuteur.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		texteReceptAuteur.setEditable(false);
		texteReceptAuteur.setBounds(50, 50, 50, 50);
		boiteRecept.add(texteReceptAuteur, BorderLayout.LINE_START);

		JTextArea texteReceptTitre = new JTextArea(10, 10);
		texteReceptTitre.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		texteReceptTitre.setEditable(false);
		texteReceptTitre.setBounds(50, 50, 50, 50);
		boiteRecept.add(texteReceptTitre, BorderLayout.CENTER);

		JTextArea texteReceptContenu = new JTextArea(10, 10);
		texteReceptContenu.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		texteReceptContenu.setEditable(false);
		texteReceptContenu.setBounds(50, 50, 50, 50);
		boiteRecept.add(texteReceptContenu, BorderLayout.LINE_END);

		afficherMessagesRecus(this.utilisateur.getIdUtilisateur(), texteReceptAuteur, texteReceptTitre,
				texteReceptContenu);

		return boiteRecept;
	}

	/**
	 * Crée la partie de boîte de messages envoyés
	 * 
	 * @return JPanel
	 * 
	 * @author Cécile
	 */
	private JPanel creerMessagesEnvoyes() {

		JPanel boiteEnvoyes = new JPanel(new BorderLayout());

		JTextArea texteEnvoyeDestinataire = new JTextArea(10, 10);
		texteEnvoyeDestinataire.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		texteEnvoyeDestinataire.setEditable(false);
		texteEnvoyeDestinataire.setBounds(10, 10, 10, 10);
		boiteEnvoyes.add(texteEnvoyeDestinataire, BorderLayout.LINE_START);

		JTextArea texteEnvoyeTitre = new JTextArea(10, 10);
		texteEnvoyeTitre.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		texteEnvoyeTitre.setEditable(false);
		texteEnvoyeTitre.setBounds(10, 10, 10, 10);
		boiteEnvoyes.add(texteEnvoyeTitre, BorderLayout.CENTER);

		JTextArea texteEnvoyeContenu = new JTextArea(10, 10);
		texteEnvoyeContenu.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		texteEnvoyeContenu.setEditable(false);
		texteEnvoyeContenu.setBounds(10, 10, 10, 10);
		boiteEnvoyes.add(texteEnvoyeContenu, BorderLayout.LINE_END);

		afficherMessagesEnvoyes(this.utilisateur.getIdUtilisateur(), texteEnvoyeDestinataire, texteEnvoyeTitre,
				texteEnvoyeContenu);

		return boiteEnvoyes;
	}

	/**
	 * Crée la partie de recherche de messages
	 * 
	 * @return JPanel
	 * 
	 * @author Cécile
	 */
	private JPanel creerRechercheMessages() {

		JPanel boiteRecherche = new JPanel(new BorderLayout());
		JPanel zoneRecherche = new JPanel(new FlowLayout());

		JButton boutonRechercher = new JButton("Rechercher");
		boutonRechercher.setName("Rechercher");
		boutonRechercher.addActionListener(this);
		zoneRecherche.add(this.barreRecherche);
		zoneRecherche.add(boutonRechercher);
		zoneRecherche.add(this.ErrorMessage);
		boiteRecherche.add(zoneRecherche, BorderLayout.PAGE_START);

		JTextArea texteRecherche = new JTextArea(10, 10);
		texteRecherche.setEditable(false);
		texteRecherche.setBounds(100, 100, 200, 40);
		boiteRecherche.add(texteRecherche);

		this.auteur.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		this.auteur.setEditable(false);
		this.auteur.setBounds(10, 10, 10, 10);
		boiteRecherche.add(this.auteur, BorderLayout.LINE_START);

		this.titre.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		this.titre.setEditable(false);
		this.titre.setBounds(10, 10, 10, 10);
		boiteRecherche.add(this.titre, BorderLayout.CENTER);

		this.contenu.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		this.contenu.setEditable(false);
		this.contenu.setBounds(10, 10, 10, 10);
		boiteRecherche.add(this.contenu, BorderLayout.LINE_END);

		return boiteRecherche;
	}

	/**
	 * Récupère les messages reçus par l'utilisateur et affiche la liste dans la
	 * boîte de réception
	 * 
	 * @param int                id : l'id de l'utilisateur dont on doit récupérer
	 *                           les messages
	 * @param texteReceptContenu
	 * @param texteReceptTitre
	 * @param texteReceptAuteur
	 * 
	 * @author Cécile
	 */
	private void afficherMessagesRecus(int utilisateurId, JTextArea texteReceptAuteur, JTextArea texteReceptTitre,
			JTextArea texteReceptContenu) {
		List<Message> messages = this.controle.recupererMessagesRecus(utilisateurId);
		try {
			if (messages.size() > 0) {
				for (Message message : messages) {
					texteReceptAuteur.append(message.getEmetteur().getNom() + "\n");
					texteReceptTitre.append(message.getSujet() + "\n");
					texteReceptTitre.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							LectureMessage lecture = new LectureMessage(message);
						}
					});
					texteReceptContenu.append(message.getContenu() + "\n");
				}
			} else {
				texteReceptTitre.setText("Aucun message reçu.");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Récupère les messages envoyés par l'utilisateur et les affiche dans la boîte
	 * d'envoi
	 * 
	 * @param utilisateurId
	 * @param texteEnvoyeDestinataire
	 * @param texteEnvoyeTitre
	 * @param texteEnvoyeContenu
	 * 
	 * @author Cécile
	 */
	private void afficherMessagesEnvoyes(int utilisateurId, JTextArea texteEnvoyeDestinataire,
			JTextArea texteEnvoyeTitre, JTextArea texteEnvoyeContenu) {
		List<Message> messages = this.controle.recupererMessagesEnvoyes(utilisateurId);
		try {
			if (messages.size() > 0) {
				for (Message message : messages) {
					texteEnvoyeDestinataire.append(message.getDestinataire().getNom() + "\n");
					texteEnvoyeTitre.append(message.getSujet() + "\n");
					texteEnvoyeTitre.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							LectureMessage lecture = new LectureMessage(message);
						}
					});
					texteEnvoyeContenu.append(message.getContenu() + "\n");
				}
			} else {
				texteEnvoyeTitre.setText("Aucun message envoyé.");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Récupère les messages recherchés par l'utilisateur et les affiche dans la
	 * boîte de recherche
	 * 
	 * @param utilisateurId : l'id de l'utilisateur dont on doit récupérer les
	 *                      messages
	 * @param recherche     : l'élément recherché par l'auteur
	 * @param auteur
	 * @param titre
	 * @param contenu
	 * 
	 * @author Cécile
	 */
	private void afficherRechercheMessages(int utilisateurId, String recherche, JTextArea auteur, JTextArea titre,
			JTextArea contenu) {

		List<Message> messages = this.controle.rechercherMessages(utilisateurId, recherche);
		try {
			if (messages.size() > 0) {
				auteur.setText("");
				titre.setText("");
				contenu.setText("");
				
				for (Message message : messages) {
					auteur.append(message.getEmetteur().getNom() + "\n");
					titre.append(message.getSujet() + "\n");
					titre.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							LectureMessage lecture = new LectureMessage(message);
						}
					});
					contenu.append(message.getContenu() + "\n");
				}
			} else {
				auteur.setText("");
				titre.setText("Aucun message trouvé.");
				contenu.setText("");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	
	@Override
	public void actionPerformed(ActionEvent action) {
		Object comp = action.getSource();

		if (comp instanceof JButton) {
			String idButton = ((JButton) action.getSource()).getName();

			if ("Rechercher".equals(idButton)) {
				if (!this.barreRecherche.getText().isEmpty()) {
					afficherRechercheMessages(this.utilisateur.getIdUtilisateur(), this.barreRecherche.getText(),
							this.auteur, this.titre, this.contenu);
				} else {
					this.ErrorMessage.setForeground(new Color(255, 0, 0));
					this.ErrorMessage.setText("Veuillez entrer un terme à rechercher.");
				}
			}

		} else if (comp instanceof JMenuItem) {

			String idMenu = ((JMenuItem) action.getSource()).getName();

			if ("nouveau".equals(idMenu)) {
				NouveauMessage nouveauMessage = new NouveauMessage();
				//nouveauMessage.envoieMessage();
				frame.dispose();
			} else if ("deco".equals(idMenu)) {
				Login login = new Login();
				login.seConecter();
				frame.dispose();

			} else if ("voirCompte".equals(idMenu)) {
				this.dispose();
				InfoUtilisateur infos = new InfoUtilisateur();

			} else if ("editerCompte".equals(idMenu)) {
				frame.dispose();
				ModifieProfilVue modifierProfil = new ModifieProfilVue();
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent pressed) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
}

///**
// * @author Sofiane
// * les actions des sous menus déconnexion et nouveau message
// * si l'utilisateur click sur Déconnexion, il sera rediréger vers la fenêtre de connexion
// * si l'utilisateur click sur nouveau message, il aura un formulaire de nouveau message
// */
//	@Override
//	public void actionPerformed11(ActionEvent e) {
//		String  source = ((JMenuItem)e.getSource()).getLabel();
//		if("Déconnexion".equals(source)) {
//			Login login = new Login();
//			login.seConecter();
//			this.frame.dispose();
//		}
//		else if("Nouveau Message".equals(source)) {
//		NouveauMessage nouveauMsg= new NouveauMessage();
//		nouveauMsg.envoieMessage();	
//		this.frame.dispose();
//
//	}
//}}
