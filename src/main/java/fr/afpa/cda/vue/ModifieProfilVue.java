/**
 * 
 */
package fr.afpa.cda.vue;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * @author ${Elvis}
 *
 */
public class ModifieProfilVue extends JPanel implements ActionListener, MouseListener, TextListener {

	JPasswordField pass = new JPasswordField(7);
	JTextField txtNom = new JTextField(15);
	JTextField txtPrenom = new JTextField(15);
	JTextField txtMail = new JTextField(15);
	JTextField txtNumTel = new JTextField(15);
	JTextField txtLogin = new JTextField(15);
	JTextField txtMdp = new JTextField(15);

	public void modiUserVue() {

		JFrame tour = new JFrame("CDA Messagerie");
		tour.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tour.setResizable(false);
		/*
		 * menuBar
		 */
		JMenuBar JMenu = new JMenuBar();
		JMenu menu1 = new JMenu("Fichier");
		JMenu menuUser = new JMenu("Utilisateur");
		JMenu menu2 = new JMenu("Help");
		JMenuItem menuitem2 = new JMenuItem("Nouveau message");
		JMenuItem menuitem3 = new JMenuItem("Connexion");
		JMenuItem menuUser1 = new JMenuItem("infos Utilisateur");
		JMenuItem menuUser2 = new JMenuItem("modification Utilisateur");
		JMenu.add(menu1);
		JMenu.add(menuUser);
		JMenu.add(menu2);
		menu1.add(menuitem2);
		menu1.add(menuitem3);
		menuUser.add(menuUser1);
		menuUser.add(menuUser2);
		tour.setJMenuBar(JMenu);
		tour.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JTextField txtNom = new JTextField(20);
		JTextField txtPrenom = new JTextField(20);
		JTextField txtMail = new JTextField(20);
		JTextField txtNumTel = new JTextField(20);
		JTextField txtLogin = new JTextField(20);
		JTextField txtMdp = new JTextField(20);

		JLabel lblNom = new JLabel("Nom", JLabel.CENTER);
		lblNom.setLabelFor(txtNom);

		JLabel lblPrenom = new JLabel("Prénom", JLabel.CENTER);
		lblPrenom.setLabelFor(txtPrenom);

		JLabel lblMail = new JLabel("Mail:", JLabel.CENTER);
		lblMail.setLabelFor(txtMail);

		JLabel lblNumTel = new JLabel("Num téléphone", JLabel.CENTER);
		lblNumTel.setLabelFor(txtNumTel);

		JLabel lblLogin = new JLabel("Login", JLabel.CENTER);
		lblLogin.setLabelFor(txtLogin);

		JLabel lblMdp = new JLabel("Mdp", JLabel.CENTER);
		lblMdp.setLabelFor(txtMdp);
		JButton valid = new JButton("Valider modification");
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(8, 8, 2, 15));
		p.add(lblNom);
		p.add(txtPrenom);
		p.add(lblMail);
		p.add(txtMail);
		p.add(lblNumTel);
		p.add(txtNumTel);
		p.add(lblLogin);
		p.add(txtLogin);
		p.add(lblMdp);
		p.add(txtMdp);
		p.add(valid);

		tour.setContentPane(p);
		tour.pack();
		tour.setVisible(true);
	}

	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
