package fr.afpa.cda.vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.afpa.cda.beans.Utilisateur;
import fr.afpa.cda.controle.Controle;
import fr.afpa.cda.controle.InscriptionControle;
import lombok.Getter;
import lombok.Setter;
/**
 * @author ${Elvis}
 *
 */
public class CreationCompte extends JFrame implements ActionListener, MouseListener, TextListener {

	JLabel ErrorMessage = new JLabel("Veuillez remplir tout les champs!!");
	JPasswordField pass = new JPasswordField(7);
	JTextField txtNom = new JTextField(15);
	JTextField txtPrenom = new JTextField(15);
	JTextField txtMail = new JTextField(15);
	JTextField txtNumTel = new JTextField(15);
	JTextField txtLogin = new JTextField(15);
	JTextField txtMdp = new JTextField(15);

	public void fenetreCre() {
		ErrorMessage.setForeground(Color.red.darker());
		JFrame tour = new JFrame();
		tour.setTitle("CDA Messagerie");
		this.setLocationRelativeTo(null);
		tour.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		

		JButton valid = new JButton("Valider");
		JLabel lblNom = new JLabel("Nom");
		JLabel lblPrenom = new JLabel("Prénom");
		JLabel lblMail = new JLabel("Mail:");
		JLabel lblNumTel = new JLabel("Num téléphone");
		JLabel lblLogin = new JLabel("Login");
		JLabel lblMdp = new JLabel("Mdp");

		JPanel panel = new JPanel();
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JPanel panel4 = new JPanel();
		JPanel panel5 = new JPanel();
		JPanel panel6 = new JPanel();
		JPanel panel7 = new JPanel();
		JPanel panel8 = new JPanel();

		panel.setLayout(new GridBagLayout());
		panel1.setLayout(new FlowLayout());
		panel1.add(lblNom);
		panel1.add(txtNom);

		panel2.setLayout(new FlowLayout());
		panel2.add(lblPrenom);
		panel2.add(txtPrenom);

		panel3.setLayout(new FlowLayout());
		panel3.add(lblMail);
		panel3.add(txtMail);

		panel4.setLayout(new FlowLayout());
		panel4.add(lblNumTel);
		panel4.add(txtNumTel);

		panel5.setLayout(new FlowLayout());
		panel5.add(lblLogin);
		panel5.add(txtLogin);

		panel6.setLayout(new FlowLayout());
		panel6.add(lblMdp);
		panel6.add(txtMdp);

		ErrorMessage.setVisible(false);
		panel.add(new Label("CDA 20156 - Messagerie"));
		panel7.add(ErrorMessage);

		panel8.setLayout(new FlowLayout());
		panel8.add(valid);

		tour.setLayout(new GridLayout(10, 1));
		tour.add(panel);
		tour.add(panel7);
		tour.add(panel1);
		tour.add(panel2);
		tour.add(panel3);
		tour.add(panel4);
		tour.add(panel5);
		tour.add(panel6);
		tour.add(panel8);

		tour.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tour.setResizable(false);
		tour.setSize(600, 600);
		tour.setVisible(true);
		valid.addActionListener(this);
	}

	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		
		String  source = ((JButton)e.getSource()).getLabel();
		if("Valider".equals(source)) {
		if(txtNom.getText().isEmpty()||txtPrenom.getText().isEmpty() ||txtMail.getText().isEmpty() 
		||txtNumTel.getText().isEmpty() ||txtLogin.getText().isEmpty()||txtMdp.getText().isEmpty()){
			ErrorMessage.setVisible(true);
			
		} else {
			InscriptionControle controle = new InscriptionControle();
			Utilisateur l = new Utilisateur();
			l.setLogin(txtLogin.getText());
			l.setNom(txtNom.getText());
			l.setPrenom(txtPrenom.getText());
			l.setMail(txtMail.getText());
			l.setNumTel(txtNumTel.getText());
			l.setMotPasse(txtMdp.getText());
			
			if (controle.controleinfos(l)==1) {
				ErrorMessage.setText("taille trop courte!");

		}else if(controle.controleinfos(l)==2){
			ErrorMessage.setText("le numéro est composé de 9 chiffres!");

		}else if(controle.controleinfos(l)==3){
			ErrorMessage.setText("adresse mail incorrect!");

		}else if(controle.controleinfos(l)==4){
			Dialog dialog= new Dialog();
			dialog.dialogNouveauCompte();

		}
		}
		
/*		
			Utilisateur verifUtil=controle.verifConnexion(saisieLogin.getText(), pass.getText());
			if(verifUtil==null) {
				saisieLogin.setText("");
				pass.setText("");
				ErrorMessage.setForeground(new Color(255,0,0));
				ErrorMessage.setText("Login ou mot de passe incorrect !!");

			} else {
				ErrorMessage.setForeground(new Color(0,128,0));
				ErrorMessage.setText("Bonjour "+ verifUtil.getNom()+" "+verifUtil.getPrenom());
			}}
		}else if("Création de compte".equals(source)) {
			this.setVisible(false);
			CreationCompte creat= new CreationCompte();
			creat.fenetreCre();
		}
	}
		
*/				
		}
		
	}

}
