package fr.afpa.cda.vue;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import fr.afpa.cda.beans.Message;
import fr.afpa.cda.controle.Controle;

/**
 * Classe permettant de lire un message sélectionné
 * 
 * @author Cécile
 *
 */
public class LectureMessage implements ActionListener{

	Controle controle = new Controle();
	Message message = new Message();
	JFrame frame = new JFrame("CDA Messagerie");

	public LectureMessage(Message message) {
		
		this.message = message;

		JFrame frame = new JFrame("CDA Messagerie");
		frame.setResizable(false);
		frame.setSize(600, 600);
		
		//---------------Menu---------------------------
		JMenuBar menu = new JMenuBar();
		JMenu fichier = new JMenu("Fichier");
		JMenu compte = new JMenu("Compte");
		JMenu help = new JMenu("Help");
		JMenuItem nouveauMess = new JMenuItem("Nouveau Message");
		nouveauMess.setName("nouveau");
		nouveauMess.addActionListener(this);
		JMenuItem deconnexion = new JMenuItem("Déconnexion");
		deconnexion.setName("deco");
		deconnexion.addActionListener(this);
		JMenuItem voirCompte = new JMenuItem("Voir le compte");
		voirCompte.setName("voirCompte");
		voirCompte.addActionListener(this);
		JMenuItem editCompte = new JMenuItem("Éditer le compte");
		editCompte.setName("editerCompte");
		editCompte.addActionListener(this);

		menu.add(fichier);
		menu.add(compte);
		menu.add(help);
		compte.add(voirCompte);
		compte.add(editCompte);
		fichier.add(nouveauMess);
		fichier.add(deconnexion);

		frame.setJMenuBar(menu);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//---------------Labels---------
		JPanel label = new JPanel(new FlowLayout());
		JLabel boiteRecept = new JLabel("Boîte de réception");
		JLabel deco = new JLabel("Déconnexion");
		label.add(boiteRecept);
		label.add(deco);
		frame.add(label, BorderLayout.PAGE_START);
		
		frame.add(afficherMessage(this.message));

		frame.setSize(800, 600);
		frame.setVisible(true);
	}

	
	/**
	 * Permet d'afficher le message sélectionné
	 * 
	 * @param message : le message à afficher
	 * @return TextArea
	 * 
	 * @author Cécile
	 */
	public TextArea afficherMessage(Message message) {
		TextArea lecture = new TextArea();
		lecture.setEditable(false);
		
		lecture.setText(message.getSujet() + "\n" + message.getContenu());
		
		return lecture;
	}

	@Override
	public void actionPerformed(ActionEvent action) {
		Object comp = action.getSource();

		if (comp instanceof JMenuItem) {

			String idMenu = ((JMenuItem) action.getSource()).getName();

			if ("nouveau".equals(idMenu)) {
				NouveauMessage nouveauMessage = new NouveauMessage();
				this.frame.dispose();
			} else if ("deco".equals(idMenu)) {
				Login login = new Login();
				login.seConecter();
				this.frame.dispose();

			} else if ("voirCompte".equals(idMenu)) {
				this.frame.dispose();
				InfoUtilisateur infos = new InfoUtilisateur();

			} else if ("editerCompte".equals(idMenu)) {
				this.frame.dispose();
				ModifieProfilVue modifierProfil = new ModifieProfilVue();
			}
		}
		
	}

}
