package fr.afpa.cda.vue;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 * Class Dialog Le Frame est une boite de dialogue qui s'affiche aprés chaque
 * action contenant le boutton de retour à la boite de messages
 * 
 * @author Sofiane
 */
public class Dialog extends JFrame implements ActionListener {

	private JButton retour = new JButton("Retour à la page d'acceuil");
	private JFrame frame = new JFrame("CDA Messagerie");

	public void dialogNouveauMsg() {

		JPanel panel = new JPanel();

		panel.add(new JLabel("Message envoyé"));
		panel.add(retour);

		GridBagLayout gbl = new GridBagLayout();
		setLayout(gbl);
		GridBagConstraints gbc = new GridBagConstraints();
		gbl.setConstraints(panel, gbc);
		add(panel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel.setBorder(BorderFactory.createTitledBorder("Envoie message : "));
		this.add(panel);
		this.setSize(400, 300);

		this.setResizable(false);
		this.setVisible(true);
		retour.addActionListener(this);

	}

	public void dialogNouveauCompte() {

		JPanel panel = new JPanel();

		panel.add(new JLabel("Compte créée avec succés : "));
		panel.add(retour);

		GridBagLayout gbl = new GridBagLayout();
		setLayout(gbl);
		GridBagConstraints gbc = new GridBagConstraints();
		gbl.setConstraints(panel, gbc);
		add(panel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel.setBorder(BorderFactory.createTitledBorder("Creation compte : "));
		this.add(panel);
		this.setSize(400, 300);

		this.setResizable(false);
		this.setVisible(true);
		retour.addActionListener(this);

	}

	/*
	 * 
	 * actionPerformed permet le retour à la boite de réception
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String source = ((JButton) e.getSource()).getLabel();
		if ("Retour à la page d'authentification".equals(source)) {
			Login login = new Login();
			login.seConecter();
			this.dispose();
		} else if ("Retour à la page d'acceuil".equals(source)) {
			BoiteReception recep = new BoiteReception(Login.verifUtil);
			this.dispose();
		}
	}

}