/**
 * 
 */
package fr.afpa.cda.vue;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * @author ${Elvis}
 *
 */
public class InfoUtilisateur extends JFrame implements ActionListener {

	JTextField txtNom = new JTextField(15);
	JTextField txtPrenom = new JTextField(15);
	JTextField txtMail = new JTextField(15);
	JTextField txtNumTel = new JTextField(15);
	JTextField txtLogin = new JTextField(15);
	JTextField txtMdp = new JTextField(15);
	JFrame tour = new JFrame();

	public InfoUtilisateur() {

		tour.setTitle("CDA Messagerie");
		this.setLocationRelativeTo(null);
		tour.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		txtNom.setEditable(false);
		txtPrenom.setEditable(false);
		txtMail.setEditable(false);
		txtNumTel.setEditable(false);
		txtMdp.setEditable(false);
		txtLogin.setEditable(false);
		txtNom.setText(Login.verifUtil.getNom());
		txtPrenom.setText(Login.verifUtil.getPrenom());
		txtMail.setText(Login.verifUtil.getMail());
		txtNumTel.setText(Login.verifUtil.getNumTel());
		txtMdp.setText(Login.verifUtil.getMotPasse());
		txtLogin.setText(Login.verifUtil.getLogin());
		
		

		JButton quit = new JButton("Quitter");
		JLabel lblNom = new JLabel("Nom");
		JLabel lblPrenom = new JLabel("Prénom");
		JLabel lblMail = new JLabel("Mail:");
		JLabel lblNumTel = new JLabel("Num téléphone");
		JLabel lblLogin = new JLabel("Login");
		JLabel lblMdp = new JLabel("Mdp");

		JPanel panel = new JPanel();
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JPanel panel4 = new JPanel();
		JPanel panel5 = new JPanel();
		JPanel panel6 = new JPanel();
		JPanel panel7 = new JPanel();
		JPanel panel8 = new JPanel();

		panel.setLayout(new GridBagLayout());
		panel1.setLayout(new FlowLayout());
		panel1.add(lblNom);
		panel1.add(txtNom);

		panel2.setLayout(new FlowLayout());
		panel2.add(lblPrenom);
		panel2.add(txtPrenom);

		panel3.setLayout(new FlowLayout());
		panel3.add(lblMail);
		panel3.add(txtMail);

		panel4.setLayout(new FlowLayout());
		panel4.add(lblNumTel);
		panel4.add(txtNumTel);

		panel5.setLayout(new FlowLayout());
		panel5.add(lblLogin);
		panel5.add(txtLogin);

		panel6.setLayout(new FlowLayout());
		panel6.add(lblMdp);
		panel6.add(txtMdp);


		panel.add(new Label("CDA 20156 - Messagerie"));


		panel8.setLayout(new FlowLayout());
		panel8.add(quit);

		tour.setLayout(new GridLayout(10, 1));
		tour.add(panel);
		tour.add(panel7);
		tour.add(panel1);
		tour.add(panel2);
		tour.add(panel3);
		tour.add(panel4);
		tour.add(panel5);
		tour.add(panel6);
		tour.add(panel8);

		tour.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tour.setResizable(false);
		tour.setSize(600, 600);
		tour.setVisible(true);
		quit.addActionListener(this);
		
	}
	


	@Override
	public void actionPerformed(ActionEvent e) {

		tour.setVisible(false);
	}
	
}
