package fr.afpa.cda.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.dom4j.CDATA;
import org.hibernate.sql.Insert;

public class AcceuilMessagerie extends JFrame implements ActionListener,TextListener{

	/**
	 * constructeur
	 */
	public AcceuilMessagerie() {
		
	}
	JPasswordField pass = new JPasswordField(7);
	JTextField txtLogin = new JTextField(15);
	JTextField txtMdp = new JTextField(15);

	public void Accueil() {

		JFrame tour = new JFrame();
		tour.setTitle("CDA Messagerie");
		this.setLocationRelativeTo(null);
		tour.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	/*
	 * menuBar 
	 */
	JMenuBar JMenu = new JMenuBar();
	JMenu menu1 = new JMenu("Fichier");
	JMenu menuUser = new JMenu("Utilisateur");
	JMenu menu2 = new JMenu("Help");
	JMenuItem menuitem2 = new JMenuItem("Nouveau message");
	JMenuItem menuitem3 = new JMenuItem("Connexion");
	JMenuItem menuUser1 = new JMenuItem("infos Utilisateur");
	JMenuItem menuUser2 = new JMenuItem("modification Utilisateur");
	JMenu.add(menu1);
	JMenu.add(menuUser);
	JMenu.add(menu2);
	menu1.add(menuitem2);
	menu1.add(menuitem3);
	menuUser.add(menuUser1);
	menuUser.add(menuUser2);
	tour.setJMenuBar(JMenu);

	JButton creat = new JButton("Création de compte");
	JButton connex = new JButton("Connexion");

	JLabel lblLogin = new JLabel("Login");
	JLabel lblMdp = new JLabel("Mdp");
	
	JPanel panelr = new JPanel();
	panelr.add(new Label("CDA 20156 - Messagerie"));
	JPanel panel = new JPanel();
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	JPanel panel3 = new JPanel();
	JPanel panel4 = new JPanel();
	JPanel paneButton = new JPanel();
	
	panel.setLayout(new GridBagLayout());
	panel1.setLayout(new FlowLayout());
	panel1.add(lblLogin);
	panel1.add(txtLogin);
	
	panel2.setLayout(new FlowLayout());
	panel2.add(lblMdp);
	panel2.add(txtMdp);
	
	paneButton.setLayout(new GridBagLayout());
	panel3.setLayout(new FlowLayout());
	panel3.add(creat);
	
	panel4.setLayout(new FlowLayout());
	panel4.add(connex);
	
	
	tour.setLayout(new GridLayout(10,1));
	tour.add(panelr);
	tour.add(panel);
	tour.add(panel1);
	tour.add(panel2);
	tour.add(panel3);
	tour.add(panel4);
	

	tour.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	tour.setResizable(false);
	tour.setSize(600, 600);
	tour.setVisible(true);
	tour.add(panel);

}	
	@Override
	public void textValueChanged(TextEvent e) {
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
	}

}
