package fr.afpa.cda.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fr.afpa.cda.controle.Controle;


/**	 
 * l'IHM d'envoie de messages
 * affiche un formulaire à l'utilisateur
 * affiche un message d'erreur si le formulaire n'est pas rempli
 * elle transmet les données à la couche contrôle si tout les champs sont remplis
 * 
 * @author Sofiane
 */
public class NouveauMessage extends JFrame implements ActionListener, TextListener{
	 
	private JButton env =new JButton("Envoyer");
	private JButton annuler =new JButton("Annuler");
	private JTextField mailObj= new JTextField(20);
	private JTextArea contenu = new JTextArea(30,30);
	private JTextField mailD= new JTextField(20);
	private JLabel errorMessage = new JLabel();
	private JFrame frame = new JFrame("CDA Messagerie");

public NouveauMessage() {
		
		JPanel label = new JPanel();
		frame.add(label, BorderLayout.PAGE_START);
		
		JTabbedPane onglets = new JTabbedPane();
		onglets.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		onglets.setBounds(30, 30, 200, 200);
		JLabel dist= new JLabel("A :    ");
	    dist.setLabelFor(mailD); 
	    JLabel obj= new JLabel("Objet :");
	    dist.setLabelFor(mailObj);
		JPanel distinataire = new JPanel();
		JPanel objet = new JPanel();
		JPanel envoye = new JPanel();
		JPanel corpsMsg = new JPanel();

		distinataire.add(dist);
		distinataire.add(mailD);
		objet.add(obj);
		objet.add(mailObj);
		envoye.add(annuler);
		envoye.add(env);

		JPanel NouveauMessage = new JPanel();
		//contenu.setBounds(0, 0, 0, 0);
		corpsMsg.add(contenu);      

		NouveauMessage.setLayout(new GridLayout(5,1));
		NouveauMessage.add(distinataire);
		NouveauMessage.add(objet);
		NouveauMessage.add(corpsMsg);
		NouveauMessage.add(errorMessage);
		NouveauMessage.add(envoye);


		//textMessEnvoyes.setText(afficherMessagesEnvoyes(2));
		
		onglets.add("Nouveau Message :", NouveauMessage);
		frame.add(onglets, BorderLayout.CENTER);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 450);
		frame.setResizable(false);
		frame.setVisible(true);
		env.addActionListener(this);
		annuler.addActionListener(this);

	}
	@Override
	public void textValueChanged(TextEvent e) {
	}
	/**
	 * les actions des bouttons Envoyer et annuler
	 * 
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String  source = ((JButton)e.getSource()).getLabel();
		/**
		 * si l'utilisateur click sur annuler
		 * il sera redérigé vers sa boite de réception
		 */
		if("Annuler".equals(source)) {
			BoiteReception boiteRecep= new BoiteReception(Login.verifUtil);
			frame.dispose();

		}
		/**
		 * Si l'utilisateur click sur le boutton envoyer 
		 * un message d'erreur s'il n'a pas rempli tout les champs
		 * un message d'erreur si l'adresse mail de destinataire est introuvable dans la bdd
		 * boite de dialogue si le message est envoyé 
		 */
		else if("Envoyer".equals(source)) {
		if(mailD.getText().isEmpty()||mailObj.getText().isEmpty()||contenu.getText().isEmpty()) {
			errorMessage.setForeground(new Color(255,0,0));
			errorMessage.setText("Veuillez remplir tout les champs!");
		}else {
			Controle controle = new Controle();	
			if(controle.controlEnvoieMessage(mailD.getText(), mailObj.getText(), contenu.getText())) {
				Dialog dialog = new Dialog();
				dialog.dialogNouveauMsg();
				frame.dispose();

			}else {
				errorMessage.setForeground(new Color(255,0,0));
				errorMessage.setText("Veuillez vérifier l'email de distenataire!");
			}
			
		}
		
	}}
}
