package fr.afpa.cda.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.cda.beans.Utilisateur;
import fr.afpa.cda.beans.Message;
import fr.afpa.cda.session.HibernateSession;


/**
 * Couche DAO, effectue les requêtes dans la base de données
 * 
 * @author BaillardMessiaenTayeb
 */
public class Dao {

	private Session session = HibernateSession.getSession();
	private Transaction transact = session.beginTransaction();

	/**
	 * methode récupérant les logins et mots de passe de la bdd
	 * 
	 * @return List Utilisateur : le résultat de la requête
	 * 
	 * @author Sofiane
	 */
	public List<Utilisateur> recupLogin() {
//		Session session = HibernateSession.getSession();
//		Transaction transact = session.beginTransaction();

		Query query = session.createQuery("SELECT util FROM Utilisateur util");

//	public String[] recupLogin() {
//		String strRecup[] = { "login", "123" };
//		return strRecup;
//	}

		// String strRecup[] = {"login","123"};
		return query.getResultList();
	}
	
	
	/**
	 * Récupère les messages reçus par l'utilisateur
	 * 
	 * @param utilisateurId : l'id de l'utilisateur pour voir ses messages reçus
	 * @return List Message : une liste des messages reçus par l'utilisateur
	 * @author Cécile
	 */
	public List<Message> recupererMessagesRecus(int utilisateurId){
		Query query = this.session.createQuery("SELECT mess FROM Message mess WHERE mess.destinataire.idUtilisateur = :utilisateurId");
		query.setParameter("utilisateurId", utilisateurId);
		return query.getResultList();
	}
	
	
	/**
	 * Récupère les messages envoyés par l'utilisateur
	 * 
	 * @param utilisateurId : l'id de l'utilisateur pour voir ses messages envoyés
	 * @return List Message : une liste des messages envoyés par l'utilisateur
	 * @author Cécile
	 */
	public List<Message> recupererMessagesEnvoyes(int utilisateurId){
		Query query = this.session.createQuery("SELECT mess FROM Message mess WHERE mess.emetteur.idUtilisateur = :utilisateurId");
		query.setParameter("utilisateurId", utilisateurId);
		return query.getResultList();
	}
	
	
	/**
	 * Récupère les messages recherchés par l'utilisateur
	 * 
	 * @param utilisateurId : l'id de l'utilisateur
	 * @param recherche : la recherche de l'utilisateur
	 * @return List Message : une liste des messages recherchés par l'utilisateur
	 * 
	 * @author Cécile
	 */
	public List<Message> rechercherMessages(int utilisateurId, String recherche){
		Query query = this.session.createQuery("SELECT mess FROM Message mess WHERE mess.emetteur.idUtilisateur = :utilisateurId AND mess.contenu LIKE :recherche OR mess.destinataire.idUtilisateur = :utilisateurId AND mess.contenu LIKE :recherche" 
	+ " OR mess.emetteur.idUtilisateur = :utilisateurId AND mess.sujet LIKE :recherche OR mess.destinataire.idUtilisateur = :utilisateurId AND mess.sujet LIKE :recherche");
		query.setParameter("utilisateurId", utilisateurId);
		query.setParameter("recherche", "%" + recherche + "%");
		return query.getResultList();
	}
	
	
	/**
	 * Récupère le message à lire
	 * 
	 * @param idMessage : l'id du message à récupérer
	 * @return Message : le message à consulter
	 * 
	 * @author Cécile
	 */
	public Message lireMessage(int idMessage) {
		Query query = this.session.createQuery("SELECT mess FROM Message mess WHERE mess.idMessage = :idMessage");
		query.setParameter("idMessage", idMessage);
		return (Message) query.getSingleResult();
	}

	/**
	 * Permet d'insérer des messages dans la base de données
	 * 
	 * @param msg le message à insérer
	 * 
	 * @author Sofiane
	 */
	public void insertMessage(Message msg) {
		
		//Query query = session.createQuery("SELECT util FROM Utilisateur util");
			session.save(msg);
			transact.commit();
	}
	
	
	/**
	 *méthode pour vérifier si le destinataire existe dans la bdd
	 *elle renvoie un utilisateur s'il existe sinon elle renvoie un null
	 * @param email : l'email du destinataire
	 * @return Utilsateur
	 *
	 *@author Sofiane
	 */
	public Utilisateur recupUtilsateur(String email) {
		Query query = this.session.createQuery("SELECT  util from Utilisateur util WHERE util.mail = :email  ");
		query.setParameter("email", email);
		if(!query.getResultList().isEmpty()) {
			return (Utilisateur) query.getSingleResult();
		}
			return null;
	}

}
