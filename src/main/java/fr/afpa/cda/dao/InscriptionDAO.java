/**
 * 
 */
package fr.afpa.cda.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;

import fr.afpa.cda.beans.Utilisateur;
import fr.afpa.cda.session.HibernateSession;

/**
 * @author ${Elvis}
 *
 */
public class InscriptionDAO {

	/*
	 * enregistrement Utilisateur
	 * ouverture session bdd
	 * sauvegarde de l'utilisateur
	 */
public void enregistrerUtilisateur(Utilisateur p)  {
		//Utilisateur per = null;
		
		Session s = null;
		
		/*
		 * Ouverture d'une session Hibernate
		 */
		s = HibernateSession.getSession();
		/*
		 * Début de la transaction
		 */
		Transaction tx = s.beginTransaction();
		/*
		 * Enregistrements de l'objet personne
		 */
		s.save(p);
		tx.commit();
	}
}
