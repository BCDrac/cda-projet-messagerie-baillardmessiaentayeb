/**
 * 
 */
package fr.afpa.cda.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.sql.Update;

import fr.afpa.cda.beans.Utilisateur;
import fr.afpa.cda.session.HibernateSession;

/**
 * @author ${Elvis}
 *
 */
public class ModifierProfilDAO {

	/**
	 * @param n : l'utilisateur
	 */
	public void enregistrerUtilisateur(Utilisateur n) {
		Utilisateur per = null;
		Session s = null;
		Transaction tx = s.beginTransaction();
		/*
		 * Ouverture d'une session Hibernate
		 */
		s = HibernateSession.getSession();

	      Query query = s.createQuery("UPDATE * from Utilisateur");

	      int rowsUpdated = query.executeUpdate();

		s.update(s);
		s.getTransaction().commit();
		s.close();
	}
}
