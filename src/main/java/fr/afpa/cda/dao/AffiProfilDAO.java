/**
 * 
 */
package fr.afpa.cda.dao;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.cda.beans.Utilisateur;
import fr.afpa.cda.session.HibernateSession;

/**
 * @author ${Elvis}
 *
 */
public class AffiProfilDAO {

	public void affiProfil() {
		Utilisateur per = null;
		Session s = null;
		/*
		 * Ouverture d'une session Hibernate
		 */
		s = HibernateSession.getSession();
		/*
		 * recupére tous les infos Utilisateur
		 */
		Query query = s.createQuery("select * from Utilisateur");
		/*
		 * Début de la transaction
		 */
		Transaction tx = s.beginTransaction();
		tx.commit();
	}
}
