package fr.afpa.cda.controle;

import java.util.List;
import java.util.ArrayList;

import fr.afpa.cda.beans.Message;
import fr.afpa.cda.beans.Utilisateur;
import fr.afpa.cda.metier.Metier;
import fr.afpa.cda.vue.Login;

/**
 * Couche de contrôle, permet de vérifier (ou transférer) des informations
 * 
 * @author BaillardMessiaenTayeb
 */
public class Controle {

	private Metier metier = new Metier();
	private Utilisateur utilisateur = new Utilisateur();

	/**
	 * Vérifie si le client a bien entré des identifiants
	 * 
	 * @param login    : le login à vérifier
	 * @param password : le mot dde passe à vérifier
	 * @return boolean : dit à la couche vue si le client a bien entré des identifiants
	 *         
	 * @author Sofiane 
	 */
	public Utilisateur verifConnexion(String login, String password) {

		ArrayList<Utilisateur> list = (ArrayList<Utilisateur>) this.metier.connexion();
		for (Utilisateur utilisateur : list) {
			if (login.equals(utilisateur.getLogin()) && password.equals(utilisateur.getMotPasse())) {
				return utilisateur;
			}
		}
		return null;
	}

	
	/**
	 * Récupère les messages reçus de l'entité utilisateur
	 * 
	 * @param utilisateurId : l'id de l'utilisateur
	 * @return List : retourne une liste des messages reçus par l'utilisateur
	 * 
	 * @author Cécile
	 */
	public List<Message> recupererMessagesRecus(int utilisateurId) {
		return this.metier.recupererMessagesRecus(utilisateurId);
	}
	
	
	/**
	 * Récupère les messages envoyés par l'entité utilisateur
	 * 
	 * @param utilisateurId : l'id de l'utilisateur
	 * @return List : retourne une liste des messages envoyés par l'utilisateur
	 * 
	 * @author Cécile
	 */
	public List<Message> recupererMessagesEnvoyes(int utilisateurId) {
		return this.metier.recupererMessagesEnvoyes(utilisateurId);
	}

	
	
	/**
	 * Récupère les messages recherchés par l'entité utilisateur
	 * 
	 * @param utilisateurId : l'id de l'utilisateur
	 * @param recherche : la recherche de l'utilisateur
	 * 
	 * @return List : retourne une liste des messages recherchés par l'utilisateur
	 * 
	 * @author Cécile
	 */
	public List<Message> rechercherMessages(int utilisateurId, String recherche){
		return this.metier.rechercherMessages(utilisateurId, recherche);
	}
	
	
	/**
	 * Méthode de contrôle de l'envoi des messages
	 * 
	 * @param distinataire : le destinataire du mesage
	 * @param sujet : le sujet du message
	 * @param contenu : le contenu du message
	 * @return boolean true si le message est envoyé sinon un false
	 * 
	 * @author Sofiane
	 */
	public boolean controlEnvoieMessage(String distinataire, String sujet, String contenu) {

		if (metier.recupUtilsateur(distinataire) != null) {

			Message msg = new Message();			
			msg.setDestinataire(metier.recupUtilsateur(distinataire));
			msg.setEmetteur(Login.verifUtil);
			msg.setContenu(contenu);
			msg.setSujet(sujet);
			metier.nouveauMessage(msg);
			return true;
		}
		return false;
	}

	public Message lireMessage(int idMessage) {
		return this.metier.lireMessage(idMessage);
	}
}
