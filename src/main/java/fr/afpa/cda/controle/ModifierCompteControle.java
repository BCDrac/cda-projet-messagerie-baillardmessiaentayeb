/**
 * 
 */
package fr.afpa.cda.controle;

import fr.afpa.cda.beans.Utilisateur;

/**
 * @author ${Elvis}
 *
 */
/*
 * controle modification de compte
 */
public class ModifierCompteControle {
	public boolean modifCompte(String login,String motPasse,String nom, 
			String prenom, String mail, String numTel) {
		Utilisateur n = new Utilisateur();
		/*
		 * controle login not null
		 * controle login différent
		 * controle la taille min 1 max 20
		 * controle du format attendu par un regex
		 */
		if ((login != null && login != n.getLogin())&&(login.length() > 0 || login.length()<= 20) && 
				(login.matches("[a-zA-Z&]{1}[a-zA-Z&-_]{1,20}"))) {
			return true;
			/*
			 * controle mot de passse not null
			 * controle mot de passse différent
			 * controle la taille min 1 max 20
			 * controle du format attendu par un regex
			 */
		}else if ((motPasse != null && motPasse != n.getMotPasse())&& (motPasse.length() > 0 || motPasse.length() <= 20)) {
			return true;
			/*
			 * controle du nom not null
			 * controle  si le nom est bien différent
			 * controle la taille min 1 max 20
			 * controle du format attendu par un regex
			 */
		}else if ((nom != null && nom != n.getNom())&&(nom.length() < 0 || nom.length()<= 20) && (nom.matches("[a-zA-Z]{1,20}"))) {
			return true;
			/*
			 * controle du prenom not null
			 * controle  si le prenom est bien différent
			 * controle la taille min 1 max 20
			 * controle du format attendu par un regex
			 */
		}else if ((prenom != null && prenom != n.getPrenom())&&(prenom.length() > 0 || prenom.length()<= 20) && 
				(prenom.matches("[a-zA-Z]{1,20}"))) {
			return true;
			/*
			 * controle du mail not null
			 * controle  si le mail est bien différent
			 * controle la taille min 1 max 20
			 * controle du format attendu par un regex
			 */
		}else if ((mail != null && mail != n.getMail() )&&(mail.length() > 0 || mail.length()<=  20) && (mail.matches(
				"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
				+ "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\"
				+ "x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
				+ "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@"
				+ "(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:"
				+ "[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?"
				+ "[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|"
				+ "[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\"
				+ "x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"))) {
					return true;
					/*
					 * controle du numéro de tel not null
					 * controle  si le numéro est bien différent
					 * controle la taille de 10 obligatoire
					 * controle du format attendu par un regex
					 */
		}else if ((numTel != null && numTel != n.getNumTel()) && numTel.length()== 10 && (numTel.matches("[0]{1}[0-9]{9}")))  {
			return true;
		}
		return false;
	}
}
