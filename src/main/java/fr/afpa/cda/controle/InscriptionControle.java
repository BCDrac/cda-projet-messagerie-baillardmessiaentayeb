/**
 * 
 */
package fr.afpa.cda.controle;

import fr.afpa.cda.beans.Utilisateur;
import fr.afpa.cda.metier.MetierInscription;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ${Elvis}
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class InscriptionControle {
	public int controleinfos(Utilisateur p) {
		
		if (p.getNom().trim().length() < 3||p.getPrenom().trim().length() < 3|| p.getLogin().trim().length() < 3 ||p.getMotPasse().trim().length() <3) {
			return 1;
		}else if(!p.getNumTel().matches("[0]{1}[0-9]{9}")){	
			return 2;
		}else if(!p.getMail().matches(
				"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
				+ "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\"
				+ "x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
				+ "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@"
				+ "(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:"
				+ "[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?"
				+ "[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|"
				+ "[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\"
				+ "x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")) {

					return 3;
		}else {
			MetierInscription mode = new MetierInscription();
			mode.enregistre(p);
			return 4;
		}

		
	}
}
