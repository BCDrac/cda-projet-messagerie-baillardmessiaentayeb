package fr.afpa.cda.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "utilisateur")
public class Utilisateur {
	@Id
	@GenericGenerator(name = "user_generator", strategy = "org.hibernate.id.enhanced.TableGenerator")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@Column(name = "idUtilisat")
	private int idUtilisateur;
	
	@Column(name = "nom", nullable = false)
	private String nom;
	@Column(name = "prenom", nullable = false)
	private String prenom;
	@Column(name = "mail", unique = true, nullable = false)
	private String mail;
	@Column(name = "numTel")
	private String numTel;
	@Column(name = "login", unique = true, nullable = false)
	private String login;
	@Column(name = "motPasse", nullable = false)
	private String motPasse;
	
	@OneToMany(mappedBy = "emetteur", cascade = { CascadeType.ALL })
	private List<Message> messageEnvoye;
	@OneToMany(mappedBy = "destinataire", cascade = { CascadeType.ALL })
	private List<Message> messageRecu;	
}