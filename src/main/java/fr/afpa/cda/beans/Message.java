package fr.afpa.cda.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "message")
public class Message {
	@Id
	@GenericGenerator(name = "message_generator", strategy = "org.hibernate.id.enhanced.TableGenerator")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "message_generator")
	@Column(name = "idMessage")
	private int idMessage;
	
	@Column(name = "sujet", nullable = false)
	private String sujet;
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "contenu", nullable = false)
	private String contenu;
	@Column(name = "lu")
	@ColumnDefault("false")
	private boolean lu;
	@ManyToOne()
	@JoinColumn(name = "idEmetteur", referencedColumnName = "idUtilisat")
	private Utilisateur emetteur;
	@ManyToOne()
	@JoinColumn(name = "idDestinataire", referencedColumnName = "idUtilisat")
	private Utilisateur destinataire;
}