package fr.afpa.cda.metier;

import java.util.List;

import fr.afpa.cda.beans.Utilisateur;
import java.util.List;

import fr.afpa.cda.beans.Message;
import fr.afpa.cda.dao.Dao;


/**
 * Couche métier, permet le traitement (ou transfer) d'informations
 * 
 * @author BaillardMessiaenTayeb
 */
public class Metier {
	
	private Dao dao = new Dao();
	
	
	/**
	 * Methode transférant les données pour la connexion utilisateur
	 * 
	 * @return String[] : le résultat de la méthode recupLogin() de la dao
	 * 
	 * @author Sofiane
	 */
	public List<Utilisateur> connexion() {
		return this.dao.recupLogin();
	}
	
	/**
	 * Méthode récupérant un message à lire
	 * 
	 * @param idMessage : l'id du message à récupérer
	 * @return Message : retourne le message que l'on souhaite consulter
	 * 
	 * @author Cécile
	 */
	public Message lireMessage(int idMessage) {
		return this.dao.lireMessage(idMessage);
	}
	
	/**
	 * Récupère les messages reçus par l'utilisateur
	 * 
	 * @param utilisateurId : l'id utilisateur dont il faut récupérer les messages reçus
	 * @return List Message : la liste de messages reçus par l'utilisateur
	 * 
	 * @author Cécile
	 */
	public List<Message> recupererMessagesRecus(int utilisateurId){
		return this.dao.recupererMessagesRecus(utilisateurId);
	}
	
	
	/**
	 * Récupère les messages envoyés par l'utilisateur
	 * 
	 * @param utilisateurId : l'id utilisateur dont il faut récupérer les messages envoyés
	 * @return List Message : la liste de messages envoyés par l'utilisateur
	 * 
	 * @author Cécile
	 */
	public List<Message> recupererMessagesEnvoyes(int utilisateurId){
		return this.dao.recupererMessagesEnvoyes(utilisateurId);
	}
	
	
	/**
	 * Récupère les messages recherchés par l'utilisateur
	 * 
	 * @param utilisateurId : l'id utilisateur dont il faut récupérer les messages recherchés
	 * @param recherche : la recherche de l'utilisateur
	 * @return List Message : la liste de messages recherchés par l'utilisateur
	 * 
	 * @author Cécile
	 */
	public List<Message> rechercherMessages(int utilisateurId, String recherche){
		return this.dao.rechercherMessages(utilisateurId, recherche);
	}
	
	
	/**
	 * méthode qui appelle la dao pour l'insertion d'un message dans la bdd
	 * 
	 * @param msg : le message à insérer
	 * 
	 * @author Sofiane
	 */
	public void nouveauMessage(Message msg) {
		
		dao.insertMessage(msg);
	}
	/**
	 * La méthode pour récupérer le destinataire en fonction d'une adresse mail
	 * @param email : l'email utilisateur
	 * @return Utilisateur
	 * 
	 * @author Sofiane
	 */
	public Utilisateur recupUtilsateur(String email) {
		return this.dao.recupUtilsateur(email);
	}
}
