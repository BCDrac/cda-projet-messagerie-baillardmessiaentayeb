/**
 * 
 */
package fr.afpa.cda.metier;

import fr.afpa.cda.beans.Utilisateur;
import fr.afpa.cda.controle.InscriptionControle;
import fr.afpa.cda.dao.InscriptionDAO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ${Elvis}
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class MetierInscription {
	/**
	 * @param p : l'utilisateur à enregistrer	 
	 */


	public void enregistre(Utilisateur p) {
		InscriptionDAO psd = new InscriptionDAO();
		psd.enregistrerUtilisateur(p);

	}
}
